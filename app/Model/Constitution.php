<?php
App::uses('AppModel', 'Model');
/**
 * Constitution Model
 *
 */
class Constitution extends AppModel {

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'body' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => '条文を入力してください',
			),
			'bio' => array(
				'rule' => array( 'between', MIN_CONSTITUTIONS_BODY, MAX_CONSTITUTIONS_BODY),
				'message' => '50文字以上、300文字以下で入力して下さい。',
			),
		),
		'approval_status' => array(
			'range' => array(
				'rule' => array('range', -1, MAX_CONSTITUTIONS_APPROVAL_STATUS),
				'message' => '0～2の値を設定してください',
			),
		),
		'chapter_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'IDを入力してください',
			),
		),
	);

	/**
	 * getConstitutionDataFindByChapterId
	 * 章IDから条文を取得する
	 *
	 * @param  int $chapter_id  章のID
	 * @return false OR ConstitutionData
	 *         章IDが与えられたときは条文を返し、与えられなかったときはfalseを返す
	 * @author mishima.masami
	 */
	public function getConstitutionDataFindByChapterId($chapterId = null) {
		$return = null;
		$approvalStatus = 1;

		if ($chapterId == null) {
			return false;
		}

		// 改憲後がある場合はそれを返し、ない場合は原文を返す
		$param = array(
			'conditions' => array(
				'Constitution.approval_status' => DEFAULT_CONSTITUTIONS_APPROVAL_STATUS,
				'Constitution.chapter_id' => $chapterId
			),
			'order' => array('id' => 'desc'),
		);
		$return = $this->find('first', $param);

		// 改憲案が存在しない場合$approvalStatusに１加算する
		if ($return == null) {
			$param = array(
				'conditions' => array(
					'Constitution.approval_status' => MAX_CONSTITUTIONS_APPROVAL_STATUS,
					'Constitution.chapter_id' => $chapterId
				)
			);
			$return = $this->find('first', $param);
		}

		return $return;
	}

	/**
	 * saveAmendmentIdea 改憲案をDBにインサート
	 *
	 * @param integer $id                                 改憲案のID
	 * @param array   $constitutionModificationIdeaData   改憲案の詳細
	 * @param array   $constitutionData                   憲法データ
	 * @return true or false
	 * @author mishima.masami
	 */
	public function saveAmendmentIdea($id, $constitutionData, $constitutionModificationIdeaData) {
		if (empty($id) || empty($constitutionData) || empty($constitutionModificationIdeaData)) {
			return false;
		}

		$constitutionData['chapter_id'] = $id;
		$constitutionFields = array('chapter_id', 'body', 'approval_status');
		$constitutionModificationIdeaField = array('title', 'reason', 'username', 'constitution_id');
		try {
			// トランザクションの開始
			$this->begin();
			if (! $this->save($constitutionData, true, $constitutionFields)) {
				throw new Exception('【' . __LINE__ . '】' . '登録に失敗しました');
			}

			// 直前にsaveしたデータのID取得
			$constitutionModificationIdeaData['constitution_id'] = $this->getLastInsertID();
			// $constitutionModificationIdeaモデルの呼び出し
			$constitutionModificationIdea = ClassRegistry::init('ConstitutionModificationIdea');
			if (! $constitutionModificationIdea->save($constitutionModificationIdeaData, true, $constitutionModificationIdeaField)) {
				throw new Exception('【' . __LINE__ . '】' . '登録に失敗しました');
			}

			// トランザクションの終了(正常)
			$this->commit();
			return true;
		} catch (Exception $e) {
			$this->log($e->getMessage(), 'error');
			// トランザクションの終了(異常)
			$this->rollback();

			return false;
		}
	}

	/**
	 * getConstitutionData idから憲法データを取得
	 *
	 * @param  integer $id  憲法ID
	 * @return true or false
	 * @author mishima.masami
	 */
	public function getConstitutionData($id) {
		$options = array('conditions' => array('Constitution.' . $this->primaryKey => $id));
		$constitutionData = $this->find('first', $options);

		if (empty($constitutionData)) {
			return false;
		}

		return $constitutionData;
	}

	/**
	 * getConstitutionModificationIdeaLists 改憲案をリストで取得する
	 *
	 * @return $IdeaLists array 改憲案のリスト
	 * @author mishima.masami
	 */
	public function getConstitutionModificationIdeaLists() {
		// 以下findのオプションの設定をする
		$option['recursive'] = -1;
		// LEFT Join Constitution & ConstitutionModificationIdea
		$options['joins'] = array(
			array(
				'type' => 'LEFT',
				'table' => 'constitution_modification_ideas',
				'alias' => 'ConstitutionModificationIdea',
				'conditions' => 'Constitution.id = ConstitutionModificationIdea.constitution_id',
			),
			array(
				'type' => 'LEFT',
				'table' => 'chapters',
				'alias' => 'Chapter',
				'conditions' => 'Chapter.id = Constitution.chapter_id',
			)
		);

		// コンディションの指定
		$options['conditions'] = array(
			'ConstitutionModificationIdea.show_status' => TRUE_CONSTITUTIONS_SHOW_STATUS,
			'Constitution.approval_status' => MIN_CONSTITUTIONS_APPROVAL_STATUS
		);

		// 取得するテーブルのカラムを指定
		$options['fields'] = array(
			'Chapter.chapter_name',
			'Constitution.id',
			'Constitution.chapter_id',
			'ConstitutionModificationIdea.title'
		);

		$ideasLists = $this->find('all', $options);

		// 改憲案が存在しなかった場合にfalseを返す
		if (empty($ideasLists)) {
			return false;
		} else {
			return $ideasLists;
		}
	}

	/**
	 * getConstitutionModificationIdea 改憲案詳細を取得
	 *
	 * @param  integer $id 改憲案のID
	 * @author mishima.masami
	 */
	public function getConstitutionModificationIdea($id = null){
		if ($id == null) {
			return false;
		}

		// 以下findのオプションの設定をする
		$option['recursive'] = -1;
		// LEFT Join Constitution & ConstitutionModificationIdea
		$options['joins'] = array(
			array(
				'type' => 'LEFT',
				'table' => 'constitution_modification_ideas',
				'alias' => 'ConstitutionModificationIdea',
				'conditions' => 'Constitution.id = ConstitutionModificationIdea.constitution_id',
			),
			array(
				'type' => 'LEFT',
				'table' => 'chapters',
				'alias' => 'Chapter',
				'conditions' => 'Chapter.id = Constitution.chapter_id',
			)
		);

		// コンディションの指定
		$options['conditions'] = array(
			'Constitution.id' => $id
		);

		// 取得するテーブルのカラムを指定
		$options['fields'] = array(
			'Chapter.chapter_name',
			'Chapter.id',
			'Chapter.title',
			'Constitution.id',
			'Constitution.body',
			'ConstitutionModificationIdea.title',
			'ConstitutionModificationIdea.reason',
			'ConstitutionModificationIdea.username'
		);

		$ideas['after'] = $this->find('first', $options);

		if (empty($ideas['after'])) {
			return false;
		}

		// 以下憲法の原文の取得
		$ideas['before'] = $this->getConstitutionDataFindByChapterId($ideas['after']['Chapter']['id']);

		if (empty($ideas['before'])) {
			return false;
		}

		return $ideas;
	}

	/**
	 * updateAuthenticationOrNonauthentication 改憲案を認証
	 *
	 * @param  integer $constitutionId 改憲案のID
	 * @param  boolean $isPermit       改憲案を通すか通さないかを指定
	 * @return ture or false
	 * @author mishima.masami
	 */
	public function updateAuthenticationOrNonauthentication($constitutionId = null, $isPermit = null) {
		if (empty($constitutionId)) {
			 return false;
		}
		if (gettype($constitutionId) != 'string') {
			return false;
		}
		if ($isPermit != 1 && $isPermit != 0) {
			return false;
		}

		try {
			// トランザクションの開始
			$this->begin();

			// 憲法改正案の更新
			$this->id = $constitutionId;
			if (! $this->saveField('approval_status', $isPermit, true)) {
				throw new Exception('【' . __LINE__ . '】' . '認証に失敗しました');
			}

			//　改憲案の詳細を保存するためにConstitutionModificationIdeaモデルを呼び出し
			$constitutionModificationIdea = ClassRegistry::init('ConstitutionModificationIdea');
			$constitutionModificationIdeaId = $constitutionModificationIdea->find('first',
				array(
					'conditions' => array(
						'ConstitutionModificationIdea.constitution_id' => $constitutionId
					),
					'fields' => 'ConstitutionModificationIdea.id'
				)
			);
			$constitutionModificationIdea->id = $constitutionModificationIdeaId;
			if (! $constitutionModificationIdea->saveField('show_status', FALSE_CONSTITUTIONS_SHOW_STATUS)) {
				throw new Exception('【' . __LINE__ . '】' . '認証に失敗しました');
			}

			// トランザクションの終了(正常)
			$this->commit();
			return true;
		} catch (Exception $e) {
			$this->log($e->getMessage(), 'error');
			// トランザクションの終了(異常)
			$this->rollback();

			return false;
		}
	}
}
