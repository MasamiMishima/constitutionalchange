<?php
App::uses('AppModel', 'Model');

/**
 * Chapter Model
 *
 */
class Chapter extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'title';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'chapter_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
	);

	/**
	 * getChapterNameList 章の名前のリストを取得
	 *
	 * @param  array constitution_data 憲法データ
	 * @return true or false
	 * @author mishima.masami
	 */
	public function getChapterNameList() {
		$chapterNameList = $this->find('list', array(
			'fields' => array('Chapter.chapter_name'),
			'recursive' => 0
		));

		return $chapterNameList;
	}
}
