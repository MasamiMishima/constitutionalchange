<?php
App::uses('AppModel', 'Model');

/**
 * ConstitutionComment Model
 *
 */
class ConstitutionComment extends AppModel {

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'body' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
			'maxLength' => array(
				'rule' => array('maxLength', MAX_CONSTITUTION_COMMENTS_BODY),
			),
		),
		'username' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
			'maxLength' => array(
				'rule' => array('maxLength', MAX_CONSTITUTION_COMMENTS_USERNAME),
			),
		),
		'show_status' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
			'boolean' => array(
				'rule' => array('boolean'),
			),
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'constitution_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
	);

	/**
	 * addComment コメントを保存する
	 *
	 * @param  $commentData     保存するコメントのデータ
	 * @param  $constitutionId  保存するコメントに対応する憲法ID
	 * @return ture or false
	 * @author mishima.masami
	 */
	public function addComment($constitutionId = null, $commentData = null) {
		if (empty($commentData) || empty($commentData)) {
			return false;
		}
		if (! is_array($commentData)) {
			return false;
		}
		$commentData['ConstitutionComment']['constitution_id'] = $constitutionId;

		if (!is_array($commentData)) {
			return false;
		}

		if (! $this->save($commentData)) {
			return false;
		}

		return true;
	}

	/**
	 * getCommentByConstitutionId コメントを保存する
	 *
	 * @param  $constitutionId 憲法IDからコメントのデータを取得する
	 * @return ture or false
	 * @author mishima.masami
	 */
	public function getCommentByConstitutionId($constitutionId = null) {
		if (empty($constitutionId)) {
			return false;
		}

		$commentData = $this->find('all', array(
			'fields' => array(
					'ConstitutionComment.username', 'ConstitutionComment.body'
			),
			'conditions' => array(
				'ConstitutionComment.constitution_id' => $constitutionId
			),
		));

		if (empty($commentData)) {
			return false;
		}
		return $commentData;
	}
}
