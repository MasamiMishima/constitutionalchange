<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
    private $dataSource;

    /**
     * begin method
     * トランザクションを開始する
     *
     * @author mishima.masami
     */
    public function begin() {
        $dataSource = ConnectionManager::getDataSource($this->useDbConfig);
        $dataSource->begin($this);
    }

    /**
     * commit method
     * トランザクションを正常終了する
     *
     * @author mishima.masami
     */
    public function commit() {
        $dataSource = ConnectionManager::getDataSource($this->useDbConfig);
        $dataSource->commit($this);
    }

    /**
     * rollback method
     * トランザクションをロールバックして終了する
     *
     * @author mishima.masami
     */
    public function rollback() {
        $dataSource = ConnectionManager::getDataSource($this->useDbConfig);
        $dataSource->rollback($this);
    }
}
