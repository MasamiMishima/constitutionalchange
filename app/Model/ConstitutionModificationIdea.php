<?php
App::uses('AppModel', 'Model');
/**
 * ConstitutionModificationIdea Model
 *
 */
class ConstitutionModificationIdea extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'title';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				// 以下コメントアウトはサンプルとして残します
				// 'message' => 'Your custom message here',
				// 'allowEmpty' => false,
				// 'required' => false,
				// 'last' => false, // Stop validation after this rule
				// 'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'タイトルをを入力してください',
			),
			'maxLength' => array(
				'rule' => array('maxLength', MAX_CONSTITUTION_MODIFICATION_IDEAS_TITLE),
				'message' => '500文字以下で入力してください',
			),
		),
		'reason' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => '理由を入力してください',
			),
			'maxLength' => array(
				'rule' => array('maxLength', MAX_CONSTITUTION_MODIFICATION_IDEAS_REASON),
				'message' => '500文字以下で入力してください',
			),
		),
		'show_status' => array(
			'boolean' => array(
				'rule' => array('boolean'),
			),
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'username' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => '名前を入力してください',
			),
			'maxLength' => array(
				'rule' => array('maxLength', MAX_CONSTITUTION_MODIFICATION_IDEAS_USERNAME),
				'message' => '10文字以下で入力してください',
			),
		),
		'constitution_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
	);
}
