<?php
App::uses('AppController', 'Controller');
/**
 * ConstitutionModificationIdeas Controller
 *
 * @property ConstitutionModificationIdea $ConstitutionModificationIdea
 * @property PaginatorComponent $Paginator
 */
class ConstitutionModificationIdeasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ConstitutionModificationIdea->recursive = 0;
		$this->set('constitutionModificationIdeas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ConstitutionModificationIdea->exists($id)) {
			throw new NotFoundException(__('Invalid constitution modification idea'));
		}
		$options = array('conditions' => array('ConstitutionModificationIdea.' . $this->ConstitutionModificationIdea->primaryKey => $id));
		$this->set('constitutionModificationIdea', $this->ConstitutionModificationIdea->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ConstitutionModificationIdea->create();
			if ($this->ConstitutionModificationIdea->save($this->request->data)) {
				$this->Session->setFlash(__('The constitution modification idea has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The constitution modification idea could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ConstitutionModificationIdea->exists($id)) {
			throw new NotFoundException(__('Invalid constitution modification idea'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ConstitutionModificationIdea->save($this->request->data)) {
				$this->Session->setFlash(__('The constitution modification idea has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The constitution modification idea could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ConstitutionModificationIdea.' . $this->ConstitutionModificationIdea->primaryKey => $id));
			$this->request->data = $this->ConstitutionModificationIdea->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ConstitutionModificationIdea->id = $id;
		if (!$this->ConstitutionModificationIdea->exists()) {
			throw new NotFoundException(__('Invalid constitution modification idea'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ConstitutionModificationIdea->delete()) {
			$this->Session->setFlash(__('The constitution modification idea has been deleted.'));
		} else {
			$this->Session->setFlash(__('The constitution modification idea could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
