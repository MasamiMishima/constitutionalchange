<?php
App::uses('AppController', 'Controller');

/**
 * Constitutions Controller
 *
 * @property Constitution $Constitution
 * @property PaginatorComponent $Paginator
 */
class ConstitutionsController extends AppController {

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator');
	var $uses = array('Chapter', 'Constitution', 'ConstitutionComment');

	/**
	 * index 憲法一覧ページ
	 *
	 * @author mishima.masami
	 */
	public function index() {
		$constitutions = null;
		$chapter = null;

		if ($this->request->is('post')) {
			// 章IDを取得
			$chapterId = $this->request->data['Constitution']['chapter_id'];
			// 章IDがあった場合は憲法データを変数に格納、それ以外のときはfalseが格納される
			$constitutions = $this->Constitution->getConstitutionDataFindByChapterId($chapterId);
		}

		//　章データの名前をリストで取得
		$chapter = $this->Chapter->getChapterNameList();

		// 表示するためのデータ
		$this->set('constitutions', $constitutions);
		$this->set('chapter', $chapter);
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add($id = null) {
		// ID check
		if (empty($id)) {
			$this->Alerts->showAlert('章を選択してください', 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}
		$constitutionData = $this->Constitution->getConstitutionData($id);
		if ($this->request->is('post')) {
			// saveAmendmentIdeaを使いデータを保存（内部でトランザクションが走ります）
			if (! $this->Constitution->saveAmendmentIdea($constitutionData['Constitution']['chapter_id'], $this->request->data['constitution'], $this->request->data['ConstitutionModificationIdea'])) {
				$this->Alerts->showAlert( '送信できませんでした。入力内容をご確認ください。', 'alert-danger');
			} else {
				$this->Alerts->showAlert('送信が完了しました。', 'alert-success');
				return $this->redirect(array('action' => 'index'));
			}
		}
		// データのセット
		$this->set('constitution', $constitutionData);
	}

	/**
	 * view method 一覧ページを表示します
	 *
	 * @param  integer $id
	 * @return void
	 */
	public function view($id = null) {
		// ID がnullだった場合憲法ページに飛ばす
		if (empty($id)) {
			$this->Alerts->showAlert('章を選択してください', 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}

		// Viewに表示するためのデータの取得とセット
		$constitutionData = $this->Constitution->getConstitutionModificationIdea($id);
		$commentData = $this->ConstitutionComment->getCommentByConstitutionId($id);
		$this->set($constitutionData);
		$this->set('comments', $commentData);
	}

	/**
	 * authenticateConstitution method 憲法の認証非認証
	 *
	 * @param integer $constitutionId 改憲案のID
	 * @param boolean $isPermit       認証するかしないか（true or false）
	 * @return Alerts
	 */
	public function authenticateConstitution($constitutionId = null, $isPermit = null) {
		if (empty($constitutionId) || !isset($isPermit)) {
			$this->Alerts->showAlert('アクセスエラー', 'alert-danger');
			return $this->redirect(array('action' => 'index'));
		}

		// 憲法の認証と非認証を変更
		if (! $this->Constitution->updateAuthenticationOrNonauthentication($constitutionId, $isPermit)) {
			$this->Alerts->showAlert('変更できませんでした。', 'alert-danger');
			return $this->redirect(array('action' => 'view', $constitutionId));
		}
		$this->Alerts->showAlert('変更が完了しました', 'alert-success');
		return $this->redirect(array('action' => 'index'));
	}

	/**
	 * showConstitutionLinst method 改憲案一覧を表示する
	 *
	 * @return void
	 */
	public function showConstitutionList() {
		$constitutionData = $this->Constitution->getConstitutionModificationIdeaLists();
		$this->set('constitutions', $constitutionData);
	}
}
