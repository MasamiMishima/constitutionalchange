<?

App::uses('Component', 'Controller');

/**
 * Alerts Component
 * アラートをまとめるコンポーネント
 *
 * @package	app.Component
 * @author mishima.masami
 */
class AlertsComponent extends Component {

	var $components = array('Session');

	/**
	 * showAlert method
	 * アラートを表示する。
	 *
	 * @param string $alert_class {alert-success, alert-info, alert-warning, alert-danger}
	 * @param string $alert_text
	 * @return void
	 * @author mishima.masami
	 */
	public function showAlert($alert_text, $alert_class) {
		$this->Session->setFlash(__($alert_text), 'alert', array(
			'plugin' => 'BoostCake',
			'class' => $alert_class
		));
	}
}
