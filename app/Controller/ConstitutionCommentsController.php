<?php
App::uses('AppController', 'Controller');
/**
 * ConstitutionComments Controller
 *
 * @property ConstitutionComment $ConstitutionComment
 * @property PaginatorComponent $Paginator
 */
class ConstitutionCommentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ConstitutionComment->recursive = 0;
		$this->set('constitutionComments', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ConstitutionComment->exists($id)) {
			throw new NotFoundException(__('Invalid constitution comment'));
		}
		$options = array('conditions' => array('ConstitutionComment.' . $this->ConstitutionComment->primaryKey => $id));
		$this->set('constitutionComment', $this->ConstitutionComment->find('first', $options));
	}

	/**
	 * add method コメントの追加
	 *
	 * @param integer $constitutionId 改憲案のID
	 * @return void
	 */
	public function add($constitutionId = null) {
		if ($constitutionId == null) {
			$this->Alerts->showAlert('投稿できません', 'alert-danger');
			return $this->redirect(array('controller' => 'Constitution', 'action' => 'index'));
		}

		if ($this->request->is('post')) {
			if (! $this->ConstitutionComment->addComment($constitutionId, $this->request->data['ConstitutionComment'])) {
				$this->Alerts->showAlert( '送信できませんでした。入力内容をご確認ください。', 'alert-danger');
				return $this->redirect(array('controller' => 'Constitutions', 'action' => 'view', $constitutionId));
			}
			$this->Alerts->showAlert('コメントの投稿が完了しました。', 'alert-success');
			return $this->redirect(array('controller' => 'Constitutions', 'action' => 'view', $constitutionId));
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ConstitutionComment->exists($id)) {
			throw new NotFoundException(__('Invalid constitution comment'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ConstitutionComment->save($this->request->data)) {
				$this->Session->setFlash(__('The constitution comment has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The constitution comment could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ConstitutionComment.' . $this->ConstitutionComment->primaryKey => $id));
			$this->request->data = $this->ConstitutionComment->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ConstitutionComment->id = $id;
		if (!$this->ConstitutionComment->exists()) {
			throw new NotFoundException(__('Invalid constitution comment'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ConstitutionComment->delete()) {
			$this->Session->setFlash(__('The constitution comment has been deleted.'));
		} else {
			$this->Session->setFlash(__('The constitution comment could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
