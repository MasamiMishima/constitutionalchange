/**
 *
 * ボタンの処理の記述
 * @author mishima.masami
 */
$(document).ready(function() {
	$('#btn').click(function() {
		$('#form').submit();
		$("#btn").prop("disabled", true);
	})
});
