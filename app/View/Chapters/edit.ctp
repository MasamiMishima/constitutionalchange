<div class="chapters form">
<?php echo $this->Form->create('Chapter'); ?>
	<fieldset>
		<legend><?php echo __('Edit Chapter'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('chapter_name');
		echo $this->Form->input('title');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Chapter.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Chapter.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Chapters'), array('action' => 'index')); ?></li>
	</ul>
</div>
