<div class="chapters form">
<?php echo $this->Form->create('Chapter'); ?>
	<fieldset>
		<legend><?php echo __('Add Chapter'); ?></legend>
	<?php
		echo $this->Form->input('chapter_name');
		echo $this->Form->input('title');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Chapters'), array('action' => 'index')); ?></li>
	</ul>
</div>
