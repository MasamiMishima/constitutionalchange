<div class="constitutions view">
	<div>
		<legend><h2><?php echo __('改憲案詳細 |' . $after['Chapter']['chapter_name'] . $after['Chapter']['title']); ?></h2></legend>
		<div class="col-md-12">
			<legend>
				<h2>
					<?php echo __('タイトル') . ':'; ?>
					<?php echo nl2br(h($after['ConstitutionModificationIdea']['title'])); ?>
				</h2>
				<h4>
					<?php echo __('投稿者') . ':'; ?>
					<?php echo nl2br(h($after['ConstitutionModificationIdea']['username'])); ?>
				</h4>
			</legend>
			<legend>
				<h2><?php echo __('現在の憲法'); ?></h2>
			</legend>
			<?php echo nl2br(h($before['Constitution']['body'])); ?>
			<legend>
				<h2><?php echo __('改憲案'); ?></h2>
			</legend>
			<p class='text-break-word'><?php echo nl2br(h($after['Constitution']['body'])); ?></p>
			<legend>
				<h3><?php echo __('理由'); ?></h3>
			</legend>
			<?php echo nl2br(h($after['ConstitutionModificationIdea']['reason'])); ?>
		</div>
		<div class="col-md-12">
			<legend>
				<h3><?php echo __('認証と非認証'); ?></h3>
			</legend>
			<div class="col-md-6">
			<?php
				echo $this->Html->link('認証', array(
						'controller' => 'Constitutions',
						'action' => 'authenticateConstitution',
						$after['Constitution']['id'],
						1
					),
					array('class' => 'btn btn-default btn-block', 'type' => 'button' )
				);
		 	?>
			</div>
			<div class="col-md-6">
			<?php
				echo $this->Html->link('非認証', array(
						'controller' => 'Constitutions',
						'action' => 'authenticateConstitution',
						$after['Constitution']['id'],
						0
					),
					array('class' => 'btn btn-default btn-block', 'type' => 'button' )
				);
		 	?>
			</div>
		</div>
	</div>
	<?php if ($comments): ?>
		<div class="col-md-12">
			<legend>
				<h3><?php echo __('コメント一覧'); ?></h3>
			</legend>
			<?php
				foreach ($Comments as $comment) {
					echo '<h5>名前:' . h($comment['ConstitutionComment']['username']) . '</h5>';
					echo 'コメント内容:' . h($comment['ConstitutionComment']['body']);
				}
			?>
		</div>
	<?php endif ?>
	<div class="col-md-12">
		<legend><?php echo __('コメント'); ?></legend>
		<?php
			echo $this->Form->create('ConstitutionComment', array(
				'inputDefaults' => array(
					'div' => 'form-group',
					'wrapInput' => false,
					'class' => 'form-control',
				),
				'url' => array(
					'controller' => 'ConstitutionComments',
					'action' => 'add',
					$after['Constitution']['id'],
				),
				'class' => 'well',
				'id' => 'form'
			));
		?>
		<fieldset>
			<?php
				echo $this->Form->input('ConstitutionComment.username', array('label' => __('名前')));
				echo $this->Form->input('ConstitutionComment.body', array('label' => __('コメント内容')));
				echo $this->Form->submit(__('提出(－－〆)'), array(
					'div' => 'form-group',
					'class' => 'btn btn-default',
					'id' => 'btn'
				));
			?>
		</fieldset>
		<?php echo $this->Form->end(); ?>
</div>
