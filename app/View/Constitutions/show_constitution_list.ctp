<div class="constitution show_constitution_list">
    <h2><?php echo __('改憲案ページ'); ?></h2>
    <?php if (!$constitutions): ?>
        <h3>データないです。</h3>
    <?php else: ?>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>章</th>
                    <th>タイトル</th>
                    <th>詳細を見る</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($constitutions as $constitution): ?>
                    <tr>
                        <td><?php echo h($constitution['Constitution']['id']); ?></td>
                        <td><?php echo h($constitution['Chapter']['chapter_name']); ?></td>
                        <td><?php echo h($constitution['ConstitutionModificationIdea']['title']); ?></td>
                        <td>
                            <?php
                                echo $this->Html->link('詳細ページ',
                                    array(
                                        'controller' => 'Constitutions',
                                        'action' => 'view',
                                        $constitution['Constitution']['id']
                                    ),
                                    array(
                                        'class' => 'btn btn-default',
                                        'type' => 'button'
                                    )
                                );
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
