<div class="constitutions form">
<?php echo $this->Form->create('Constitution'); ?>
	<fieldset>
		<legend><?php echo __('Edit Constitution'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('body');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Constitution.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Constitution.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Constitutions'), array('action' => 'index')); ?></li>
	</ul>
</div>
