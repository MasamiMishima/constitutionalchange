<div class="constitutions index">
	<h2><?php echo __('憲法一覧ページ'); ?></h2>
	<div class="col-md-12">
		<?php
			echo $this->Form->create('Constitution', array('class' => 'form-group', 'id' => 'form'));
			echo $this->Form->input( 'chapter_id',
				array(
					'label' => false,
					'type' => 'select',
					'options' => $chapter,
					'class' => 'form-control',
				array(
					'empty' => __('選択してください')
				)));
			echo $this->Form->submit(__('閲覧'),
				array(
					'div' => false,
					'id' => 'btn',
					'class' => 'btn btn-primary',
				)
			);
			echo $this->Form->end();
		?>
	</div>
	<div class="col-md-12">
		<?php
			if (!empty($constitutions)) {
				echo '<div class="col-md-12"><p class="text-break-word">', nl2br(h($constitutions['Constitution']['body'])) . '</p></div>';
				echo $this->Html->link(
					$this->Html->tag('span', __('改憲したい(－－〆)'),
						array('class' => 'glyphicon glyphicon-pencil')
					),
					array('action' => 'add', $constitutions['Constitution']['id']),
					array('escape' => false, 'class' => 'btn btn-primary col-md-12', 'id' => 'btn', 'role' => 'button'));
			} else {
				echo __('章を選択してください');
			}
		?>
	</div>
</div>
