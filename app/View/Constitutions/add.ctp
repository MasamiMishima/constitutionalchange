<div class="constitutions form">
	<div class="col-md-12">
		<h2><?php echo __('現在の憲法'); ?></h2>
		<?php echo nl2br(h($constitution['Constitution']['body'])); ?>
		&nbsp;
	</div>
	<div class="col-md-12">
		<?php
			echo $this->Form->create('Constitution', array(
				'inputDefaults' => array(
					'div' => 'form-group',
					'wrapInput' => false,
					'class' => 'form-control',
				),
				'class' => 'well',
				'id' => 'form'
			));
		?>
		<fieldset>
			<legend><?php echo __('改憲案を提出する'); ?></legend>
			<?php
				echo $this->Form->input('ConstitutionModificationIdea.username', array('label' => __('名前')));
				echo $this->Form->input('ConstitutionModificationIdea.title', array('label' => __('タイトル')));
				echo $this->Form->input('ConstitutionModificationIdea.reason', array('label' => __('理由')));
				echo $this->Form->input('constitution.body', array('label' => __('改憲案')));
				echo $this->Form->submit(__('提出(－－〆)'), array(
					'div' => 'form-group',
					'class' => 'btn btn-default',
					'id' => 'btn'
				));
			?>
		</fieldset>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
