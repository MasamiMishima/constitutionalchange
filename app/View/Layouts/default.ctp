<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
	echo $this->Html->meta('icon');

	// echo $this->Html->css('cake.generic');
	echo $this->Html->script('//code.jquery.com/jquery-1.10.2.min.js');

	// Twitter Bootstrap 3.0 CDN
	echo $this->Html->css('bootstrap.min');
	echo $this->Html->script('bootstrap.min');

	echo $this->Html->script('base');
	echo $this->Html->css('base');

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>
</head>
<body>
	<div id="container">
			<div class="navbar navbar-inverse navbar-static-top">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<?php echo $this->Html->link('改憲したいな(－－〆)(仮)',
						array(
							'controller' => 'Constitutions', 'action' => 'index'
						),
						array(
							'class' => 'navbar-brand'
						)
					); ?>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><?php
							echo $this->Html->link('憲法一覧', array(
								'controller' => 'Constitutions', 'action' => 'index'
							));
						?></li>
						<li><?php
							echo $this->Html->link('改憲案一覧', array(
								'controller' => 'Constitutions', 'action' => 'showConstitutionList'
							));
						?></li>
					</ul>
				</div>
			</div>
		<div id="content">

			<?php echo __($this->Session->flash()); ?>
			<?php echo $this->fetch('content'); ?>

		</div>
	</div>
	<section id="footer-sec" >
		<div class="container">
			<div class="row pad-bottom" >
				<div class="col-md-4">
					<h4> <strong>© 2015 ~~~~~</strong> </h4>
				</div>
			</div>
		</div>
	</section>
</body>
</html>
