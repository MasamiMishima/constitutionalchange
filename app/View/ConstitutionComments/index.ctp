<div class="constitutionComments index">
	<h2><?php echo __('Constitution Comments'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('body'); ?></th>
			<th><?php echo $this->Paginator->sort('username'); ?></th>
			<th><?php echo $this->Paginator->sort('show_status'); ?></th>
			<th><?php echo $this->Paginator->sort('constitution_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($constitutionComments as $constitutionComment): ?>
	<tr>
		<td><?php echo h($constitutionComment['ConstitutionComment']['id']); ?>&nbsp;</td>
		<td><?php echo h($constitutionComment['ConstitutionComment']['body']); ?>&nbsp;</td>
		<td><?php echo h($constitutionComment['ConstitutionComment']['username']); ?>&nbsp;</td>
		<td><?php echo h($constitutionComment['ConstitutionComment']['show_status']); ?>&nbsp;</td>
		<td><?php echo h($constitutionComment['ConstitutionComment']['constitution_id']); ?>&nbsp;</td>
		<td><?php echo h($constitutionComment['ConstitutionComment']['created']); ?>&nbsp;</td>
		<td><?php echo h($constitutionComment['ConstitutionComment']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $constitutionComment['ConstitutionComment']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $constitutionComment['ConstitutionComment']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $constitutionComment['ConstitutionComment']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $constitutionComment['ConstitutionComment']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Constitution Comment'), array('action' => 'add')); ?></li>
	</ul>
</div>
