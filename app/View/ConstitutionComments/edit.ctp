<div class="constitutionComments form">
<?php echo $this->Form->create('ConstitutionComment'); ?>
	<fieldset>
		<legend><?php echo __('Edit Constitution Comment'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('body');
		echo $this->Form->input('username');
		echo $this->Form->input('show_status');
		echo $this->Form->input('constitution_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ConstitutionComment.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('ConstitutionComment.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Constitution Comments'), array('action' => 'index')); ?></li>
	</ul>
</div>
