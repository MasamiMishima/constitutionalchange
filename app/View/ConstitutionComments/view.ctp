<div class="constitutionComments view">
<h2><?php echo __('Constitution Comment'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($constitutionComment['ConstitutionComment']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Body'); ?></dt>
		<dd>
			<?php echo h($constitutionComment['ConstitutionComment']['body']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($constitutionComment['ConstitutionComment']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Show Status'); ?></dt>
		<dd>
			<?php echo h($constitutionComment['ConstitutionComment']['show_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Constitution Id'); ?></dt>
		<dd>
			<?php echo h($constitutionComment['ConstitutionComment']['constitution_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($constitutionComment['ConstitutionComment']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($constitutionComment['ConstitutionComment']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Constitution Comment'), array('action' => 'edit', $constitutionComment['ConstitutionComment']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Constitution Comment'), array('action' => 'delete', $constitutionComment['ConstitutionComment']['id']), array(), __('Are you sure you want to delete # %s?', $constitutionComment['ConstitutionComment']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Constitution Comments'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Constitution Comment'), array('action' => 'add')); ?> </li>
	</ul>
</div>
