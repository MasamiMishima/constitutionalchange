<div class="constitutionModificationIdeas form">
<?php echo $this->Form->create('ConstitutionModificationIdea'); ?>
	<fieldset>
		<legend><?php echo __('Edit Constitution Modification Idea'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title');
		echo $this->Form->input('reason');
		echo $this->Form->input('show_status');
		echo $this->Form->input('username');
		echo $this->Form->input('constitution_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ConstitutionModificationIdea.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('ConstitutionModificationIdea.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Constitution Modification Ideas'), array('action' => 'index')); ?></li>
	</ul>
</div>
