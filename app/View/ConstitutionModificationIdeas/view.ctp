<div class="constitutionModificationIdeas view">
<h2><?php echo __('Constitution Modification Idea'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($constitutionModificationIdea['ConstitutionModificationIdea']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($constitutionModificationIdea['ConstitutionModificationIdea']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Reason'); ?></dt>
		<dd>
			<?php echo h($constitutionModificationIdea['ConstitutionModificationIdea']['reason']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Show Status'); ?></dt>
		<dd>
			<?php echo h($constitutionModificationIdea['ConstitutionModificationIdea']['show_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($constitutionModificationIdea['ConstitutionModificationIdea']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Constitution Id'); ?></dt>
		<dd>
			<?php echo h($constitutionModificationIdea['ConstitutionModificationIdea']['constitution_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($constitutionModificationIdea['ConstitutionModificationIdea']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($constitutionModificationIdea['ConstitutionModificationIdea']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Constitution Modification Idea'), array('action' => 'edit', $constitutionModificationIdea['ConstitutionModificationIdea']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Constitution Modification Idea'), array('action' => 'delete', $constitutionModificationIdea['ConstitutionModificationIdea']['id']), array(), __('Are you sure you want to delete # %s?', $constitutionModificationIdea['ConstitutionModificationIdea']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Constitution Modification Ideas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Constitution Modification Idea'), array('action' => 'add')); ?> </li>
	</ul>
</div>
