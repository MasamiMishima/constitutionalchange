<?php
App::uses('Constitution', 'Model');

/**
 * Constitution Test Case
 *
 */
class ConstitutionTest extends CakeTestCase {

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = array(
		'app.constitution',
		'app.constitution_modification_idea',
		'app.chapter'
	);

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp() {
		parent::setUp();
		$this->Constitution = ClassRegistry::init('Constitution');
	}

	/**
	 * testSaveAmendmentIdeaOkメソッドのテスト(正常系)
	 *
	 * @return void
	 * @author mishima.masami
	 */
	public function testSaveAmendmentIdeaOk() {
		$constitutionIdOk = 1;
		$constitutionDataOk = array(
			'chapter_id' => 1,
			'body' => $this->__outputCharacter(MIN_CONSTITUTIONS_BODY),
		);

		$constitutionModificationIdeaDataOk = array(
			'title' => $this->__outputCharacter(MIN_CONSTITUTION_MODIFICATION_IDEAS_TITLE),
			'reason' => $this->__outputCharacter(MIN_CONSTITUTION_MODIFICATION_IDEAS_REASON),
			'username' => $this->__outputCharacter(MIN_CONSTITUTION_MODIFICATION_IDEAS_USERNAME)
		);

		$constitutionDataNg = $constitutionDataOk;
		$constitutionModificationIdeaDataNg = $constitutionModificationIdeaDataOk;

		// ------タイトルについてのテスト--------//
		// タイトルが最小文字数よりも多いとき
		$constitutionModificationIdeaDataOk['title'] = $this->__outputCharacter(MIN_CONSTITUTION_MODIFICATION_IDEAS_TITLE);
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataOk, $constitutionModificationIdeaDataOk);
		$this->assertEquals(true, $result);

		// タイトルが最大文字数以内のとき
		$constitutionModificationIdeaDataOk['title'] = $this->__outputCharacter(MAX_CONSTITUTION_MODIFICATION_IDEAS_TITLE);
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataOk, $constitutionModificationIdeaDataOk);
		$this->assertEquals(true, $result);

		// ------名前についてのテスト--------//
		// 名前が最小文字数よりも多いとき
		$constitutionModificationIdeaDataOk['username'] = $this->__outputCharacter(MIN_CONSTITUTION_MODIFICATION_IDEAS_USERNAME);
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataOk, $constitutionModificationIdeaDataOk);
		$this->assertEquals(true, $result);

		// 名前が最大文字数以内のとき
		$constitutionModificationIdeaDataOk['username'] = $this->__outputCharacter(MAX_CONSTITUTION_MODIFICATION_IDEAS_USERNAME);
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataOk, $constitutionModificationIdeaDataOk);
		$this->assertEquals(true, $result);

		// --------------理由についてのテスト------//
		// 理由が最小文字数よりも多いとき
		$constitutionModificationIdeaDataOk['reason'] = $this->__outputCharacter(MIN_CONSTITUTION_MODIFICATION_IDEAS_REASON);
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataOk, $constitutionModificationIdeaDataOk);
		$this->assertEquals(true, $result);

		// 理由が最大文字数以内のとき
		$constitutionModificationIdeaDataOk['reason'] = $this->__outputCharacter(MAX_CONSTITUTION_MODIFICATION_IDEAS_REASON);
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataOk, $constitutionModificationIdeaDataOk);
		$this->assertEquals(true, $result);

		// --------------改憲内容についてのテスト------//
		// 改憲内容が最上文字数以上のとき
		$constitutionDataOk['body'] = $this->__outputCharacter(MIN_CONSTITUTIONS_BODY);
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataOk, $constitutionModificationIdeaDataOk);
		$this->assertEquals(true, $result);

		// 改憲内容が最大文字数以内のとき
		$constitutionDataOk['body'] = $this->__outputCharacter(MAX_CONSTITUTIONS_BODY);
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataOk, $constitutionModificationIdeaDataOk);
		$this->assertEquals(true, $result);
	}

	/**
	 * testSaveAmendmentIdeaNgメソッドのテスト(異常系)
	 * @todo 定数でそれぞれのバリデーションをチェックする
	 * @return void
	 * @author mishima.masami
	 */
	public function testSaveAmendmentIdeaNg() {
		$constitutionIdOk = 1;
		$constitutionDataOk = array(
			'chapter_id' => 1,
			'body' => $this->__outputCharacter(MIN_CONSTITUTIONS_BODY),
		);

		$constitutionModificationIdeaDataOk = array(
			'title' => $this->__outputCharacter(MIN_CONSTITUTION_MODIFICATION_IDEAS_TITLE),
			'reason' => $this->__outputCharacter(MIN_CONSTITUTION_MODIFICATION_IDEAS_REASON),
			'username' => $this->__outputCharacter(MIN_CONSTITUTION_MODIFICATION_IDEAS_USERNAME)
		);

		$constitutionIdNg = $constitutionIdOk;
		$constitutionDataNg = $constitutionDataOk;
		$constitutionModificationIdeaDataNg = $constitutionModificationIdeaDataOk;

		// ------引数が空のとき-------//
		// すべての引数が空のとき
		$result = $this->Constitution->saveAmendmentIdea(null, null, null);
		$this->assertEquals(false, $result);

		// 第1引数が空のとき
		$result = $this->Constitution->saveAmendmentIdea(null, $constitutionDataOk, $constitutionModificationIdeaDataOk);
		$this->assertEquals(false, $result);

		// 第2引数が空のとき
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, null, $constitutionModificationIdeaDataOk);
		$this->assertEquals(false, $result);

		// 第3引数が空のとき
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionModificationIdeaDataOk, null);
		$this->assertEquals(false, $result);

		// ----------IDについてのテスト----------//
		// IDがnullのとき
		$constitutionIdNg = '';
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdNg, $constitutionDataOk, $constitutionModificationIdeaDataOk);
		$this->assertEquals(false, $result);

		// IDが文字列のとき
		$constitutionIdNg = 'kldfsahflkhs';
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdNg, $constitutionDataOk, $constitutionModificationIdeaDataOk);
		$this->assertEquals(false, $result);

		// --------名前についてのテスト-----------//
		// 名前がnullのとき
		$constitutionModificationIdeaDataNg['username'] = null;
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataOk, $constitutionModificationIdeaDataNg);
		$this->assertEquals(false, $result);
		$constitutionModificationIdeaDataNg = $constitutionModificationIdeaDataOk;

		// 名前が最大文字数以上のときのとき
		$constitutionModificationIdeaDataNg['username'] = $this->__outputCharacter(MAX_CONSTITUTION_MODIFICATION_IDEAS_USERNAME + 1);
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataOk, $constitutionModificationIdeaDataNg);
		$this->assertEquals(false, $result);
		$constitutionModificationIdeaDataNg = $constitutionModificationIdeaDataOk;

		// --------タイトルについてのテスト-----------//
		// タイトルがnullのとき
		$constitutionModificationIdeaDataNg['title'] = null;
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataOk, $constitutionModificationIdeaDataNg);
		$this->assertEquals(false, $result);
		$constitutionModificationIdeaDataNg = $constitutionModificationIdeaDataOk;

		// タイトルが最大文字数以上のとき
		$constitutionModificationIdeaDataNg['title'] = $this->__outputCharacter(MAX_CONSTITUTION_MODIFICATION_IDEAS_TITLE + 1);
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataOk, $constitutionModificationIdeaDataNg);
		$this->assertEquals(false, $result);
		$constitutionModificationIdeaDataNg = $constitutionModificationIdeaDataOk;

		// --------理由についてのテスト-----------//
		// 理由がnullのとき
		$constitutionModificationIdeaDataNg['reason'] = null;
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataOk, $constitutionModificationIdeaDataNg);
		$this->assertEquals(false, $result);
		$constitutionModificationIdeaDataNg = $constitutionModificationIdeaDataOk;

		// 理由が最大文字数以上のとき
		$constitutionModificationIdeaDataNg['reason'] = $this->__outputCharacter(MAX_CONSTITUTION_MODIFICATION_IDEAS_REASON + 1);
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataOk, $constitutionModificationIdeaDataNg);
		$this->assertEquals(false, $result);
		$constitutionModificationIdeaDataNg = $constitutionModificationIdeaDataOk;

		// --------改憲案についてのテスト-----------//
		// 改憲案がnullのとき
		$constitutionDataNg['body'] = null;
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataNg, $constitutionModificationIdeaDataOk);
		$this->assertEquals(false, $result);
		$constitutionDataNg = $constitutionDataOk;

		// 改憲案が最小文字数以下のとき
		$constitutionDataNg['body'] = $this->__outputCharacter(MIN_CONSTITUTIONS_BODY - 1);
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataNg, $constitutionModificationIdeaDataOk);
		$this->assertEquals(false, $result);
		$constitutionDataNg = $constitutionDataOk;

		// 改憲案が最大文字数以上のとき
		$constitutionDataNg['body'] = $this->__outputCharacter(MAX_CONSTITUTIONS_BODY + 1);
		$result = $this->Constitution->saveAmendmentIdea($constitutionIdOk, $constitutionDataNg, $constitutionModificationIdeaDataOk);
		$this->assertEquals(false, $result);
		$constitutionDataNg = $constitutionDataOk;
	}

	/**
	 * getConstitutionDataメソッドのテスト
	 *
	 * @return void
	 * @author mishima.masami
	 */
	public function testGetConstitutionData() {
		$result = $this->Constitution->getConstitutionData(null);
		$this->assertEquals(false, $result);

		//正常値をセット
		$constitutionDataOk = array(
			'Constitution' => array(
				'body' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi',
			));

		$result = $this->Constitution->getConstitutionData(1);
		$this->assertEquals(1, count($result));
		$this->assertEquals(true, !empty($result));
		$this->assertEquals($constitutionDataOk['Constitution']['body'], $result['Constitution']['body']);
	}

	/**
	 * testGetConstitutionDataFindByChapterIdメソッドのテスト
	 *
	 * @return void
	 * @author mishima.masami
	 */
	public function testGetConstitutionDataFindByChapterId(){
		// 引数が正しいかをテスト
		$result = $this->Constitution->getConstitutionDataFindByChapterId(null);
		$this->assertEquals(false, $result);

		// 改憲後のデータを取得しているかをテスト
		$result = $this->Constitution->getConstitutionDataFindByChapterId(2);
		$testData = array(
			'id' => 3,
			'body' => 'Lorem ipsum dolor sit amet,aaaaaaaaaaaaaaaaaaaa',
			'chapter_id' => 2,
			'approval_status' => 1,
			'created' => '2015-08-18 14:10:24',
			'modified' => '2015-08-18 14:10:24'
		);
		$this->assertEquals(1, count($result));
		$this->assertEquals(true, !empty($result));
		$this->assertEquals($testData, $result['Constitution']);

		// 改憲後のデータを取得しているかをテスト
		$result = $this->Constitution->getConstitutionDataFindByChapterId(1);
		$testData = array(
			'id' => 1,
			'body' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi',
			'chapter_id' => 1,
			'approval_status' => 2,
			'created' => '2015-08-18 14:10:24',
			'modified' => '2015-08-18 14:10:24'
		);
		$this->assertEquals(1, count($result));
		$this->assertEquals(true, !empty($result));
		$this->assertEquals($testData, $result['Constitution']);
	}

	/**
	 * testGetConstitutionModificationIdeaListsメソッドのテスト
	 *
	 * @return void
	 * @author mishima.masami
	 */
	public function testGetConstitutionModificationIdeaLists(){
		$result = $this->Constitution->getConstitutionModificationIdeaLists();
		$testData = array(
			'0' => array(
				'Constitution' => array(
					'id' => '5',
					'chapter_id' => '5'
				),
				'ConstitutionModificationIdea' => array(
					'title' => 'Lorem ipsum dolor sit amet'
				),
				'Chapter' => array(
					'chapter_name' => '第5章'
				),
			),
			'1' => array(
				'Constitution' => array(
					'id' => '4',
					'chapter_id' => '6'
				),
				'ConstitutionModificationIdea' => array(
					'title' => 'Lorem ipsum dolor sit amet'
				),
				'Chapter' => array(
					'chapter_name' => '第6章'
				),
			),
		);

		$this->assertEquals(true, !empty($result));
		$this->assertEquals($testData, $result);
	}

	/**
	 * testGetConstitutionModificationIdeaメソッドのテスト
	 *
	 * @return void
	 * @author mishima.masami
	 */
	public function testGetConstitutionModificationIdea() {
		$result = $this->Constitution->getConstitutionModificationIdea(null);
		$this->assertEquals(false, $result);

		$result = $this->Constitution->getConstitutionModificationIdea('5');
		$result = $this->Constitution->getConstitutionModificationIdea('1');
		$testData = array(
			'after' => array(
				'Chapter' => array(
					'chapter_name' => '第1章',
					'id' => '1',
					'title' => '天皇',
				),
				'Constitution' => array(
					'id' => '1',
					'body' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi'
				),
				'ConstitutionModificationIdea' => array(
					'title' => 'Lorem ipsum dolor sit amet',
					'reason' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
					'username' => 'Lorem ipsum dolor sit amet',
				),
			),
			'before' => array(
				'Constitution' => array(
					'id' => '1',
					 'body' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi',
					'approval_status' => '2',
					'chapter_id' => '1',
					'created' => '2015-08-18 14:10:24',
					'modified' => '2015-08-18 14:10:24',
				),
			),
		);
		$this->assertEquals($testData, $result);
	}

	/**
	 *updateAuthenticationOrNonauthentication メソッドのテスト
	 *
	 * @return void
	 * @author mishima.masami
	 */
	public function testUpdateAuthenticationOrNonauthentication() {
		// 引数がすべてnullのとき
		$constitutionIdOk = 4;
		$isPermitOk = FALSE_IS_PERMIT;
		$constitutionIdNg = '';
		$isPermitNg = '';
		// -------引数がnullのとき ---------//
		// 第1第2引数がnullのとき
		$result = $this->Constitution->updateAuthenticationOrNonauthentication($constitutionIdNg, $isPermitNg);
		$this->assertEquals(false, $result);

		// 第1引数がnullのとき
		$result = $this->Constitution->updateAuthenticationOrNonauthentication($constitutionIdNg, $isPermitOk);
		$this->assertEquals(false, $result);

		// 第2引数がnullのとき
		$result = $this->Constitution->updateAuthenticationOrNonauthentication($constitutionIdOk, $isPermitNg);
		$this->assertEquals(false, $result);

		// --------第1引数についてテスト ------//
		// 引数が文字列の場合
		$constitutionIdNg = 'テスト';
		$result = $this->Constitution->updateAuthenticationOrNonauthentication($constitutionIdNg, $isPermitOk);
		$this->assertEquals(false, $result);

		// --------第2引数についてテスト ------//
		// 引数が文字列の場合
		$isPermitNg = 'テスト';
		$result = $this->Constitution->updateAuthenticationOrNonauthentication($constitutionIdOk, $isPermitNg);
		$this->assertEquals(false, $result);

		// 2以上のとき
		$isPermitNg = TRUE_IS_PERMIT + 1;
		$result = $this->Constitution->updateAuthenticationOrNonauthentication($constitutionIdOk, $isPermitNg);
		$this->assertEquals(false, $result);

		// -----引数が正常の場合のテスト --------//
		// isPermitが0(false)の場合
		$isPermitOk = FALSE_IS_PERMIT;
		$result = $this->Constitution->updateAuthenticationOrNonauthentication($constitutionIdOk, $isPermitOk);
		$this->assertEquals(true, $result);

		// isPermitが1(true)の場合
		$isPermitOk = TRUE_IS_PERMIT;
		$result = $this->Constitution->updateAuthenticationOrNonauthentication($constitutionIdOk, $isPermitOk);
		$this->assertEquals(true, $result);
	}

	/**
	 * __outputCharacter
	 *
	 * @param  string  $characterNumber  文字数
	 * @return $chart  指定された文字数の文字
	 * @author mishima.masami
	 */
	private function __outputCharacter($characterNumber) {
		return str_repeat('a', $characterNumber);
	}

	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown() {
		unset($this->Constitution);

		parent::tearDown();
	}
}
