<?php
App::uses('ConstitutionModificationIdea', 'Model');

/**
 * ConstitutionModificationIdea Test Case
 *
 */
class ConstitutionModificationIdeaTest extends CakeTestCase {

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = array(
		'app.constitution_modification_idea'
	);

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp() {
		parent::setUp();
		$this->ConstitutionModificationIdea = ClassRegistry::init('ConstitutionModificationIdea');
	}

	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown() {
		unset($this->ConstitutionModificationIdea);

		parent::tearDown();
	}
}
