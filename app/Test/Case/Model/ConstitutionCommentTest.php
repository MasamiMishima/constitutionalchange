<?php
App::uses('ConstitutionComment', 'Model');

/**
 * ConstitutionComment Test Case
 *
 */
class ConstitutionCommentTest extends CakeTestCase {

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = array(
		'app.constitution_comment'
	);

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp() {
		parent::setUp();
		$this->ConstitutionComment = ClassRegistry::init('ConstitutionComment');
	}

	/**
	 * testAddComment method
	 * AddCommentのテスト
	 * @return void
	 */
	public function testAddComment() {
		$addDataOk = array(
			'ConstitutionComment' => array(
				'body' => $this->__outputCharacter(MAX_CONSTITUTION_COMMENTS_BODY),
				'username' => $this->__outputCharacter(MAX_CONSTITUTION_COMMENTS_USERNAME),
				'show_status' => TRUE_CONSTITUTION_COMMENTS_STATUS,
			)
		);
		$addDataNg = $addDataOk;
		$constitutionIdOk = 1;

		// 以下addCommentの引数のチェックをします
		// addCommentに引数を渡さなかった場合
		$result = $this->ConstitutionComment->addComment();
		$this->assertEquals(false, $result);

		// ---------第1引数のテスト-------//
		// addCommentの第1引数がnullの場合
		$result = $this->ConstitutionComment->addComment('', $addDataOk);
		$this->assertEquals(false, $result);

		// addCommentの第1引数が文字列の場合
		$result = $this->ConstitutionComment->addComment($this->__outputCharacter(3), $addDataOk);
		$this->assertEquals(false, $result);

		// ---------第2引数のテスト-------//
		// addCommentの第2引数がnullの場合
		$result = $this->ConstitutionComment->addComment($constitutionIdOk, '');
		$this->assertEquals(false, $result);

		// addCommentの第2引数が文字列の場合
		$result = $this->ConstitutionComment->addComment($constitutionIdOk, $this->__outputCharacter(3));
		$this->assertEquals(false, $result);

		// 以下第2引数の配列の中身についてのテスト
		// 第2引数の配列のbodyの値が間違っているとき
		// bodyがNullのとき
		$addDataNg['ConstitutionComment']['body'] = null;
		$result = $this->ConstitutionComment->addComment($constitutionIdOk, $addDataNg);
		$this->assertEquals(false, $result);

		// bodyが最大文字数以上のとき
		$addDataNg['ConstitutionComment']['body'] = $this->__outputCharacter(MAX_CONSTITUTION_COMMENTS_BODY + 1);
		$result = $this->ConstitutionComment->addComment($constitutionIdOk, $addDataNg);
		$this->assertEquals(false, $result);

		// usernameの値が間違っているとき
		// usernameがnullのとき
		$addDataNg = $addDataOk;
		$addDataNg['ConstitutionComment']['username'] = '';
		$result = $this->ConstitutionComment->addComment($constitutionIdOk, $addDataNg);
		$this->assertEquals(false, $result);

		// usernameが最大文字数以上のとき
		$addDataNg['ConstitutionComment']['username'] = $this->__outputCharacter(MAX_CONSTITUTION_COMMENTS_USERNAME + 1);
		$result = $this->ConstitutionComment->addComment($constitutionIdOk, $addDataNg);
		$this->assertEquals(false, $result);

		// show_statusの値が間違っているとき
		// show_statusがnullのとき
		$addDataNg = $addDataOk;
		$addDataNg['ConstitutionComment']['show_status'] = '';
		$result = $this->ConstitutionComment->addComment($constitutionIdOk, $addDataNg);
		$this->assertEquals(false, $result);

		// show_statusが0,1以外の数字のとき
		$addDataNg['ConstitutionComment']['show_status'] = 2;
		$result = $this->ConstitutionComment->addComment($constitutionIdOk, $addDataNg);
		$this->assertEquals(false, $result);

		// show_statusが文字のとき
		$addDataNg['ConstitutionComment']['show_status'] = $this->__outputCharacter(3);
		$result = $this->ConstitutionComment->addComment($constitutionIdOk, $addDataNg);
		$this->assertEquals(false, $result);

		// 引数の値が正常の場合
		$result = $this->ConstitutionComment->addComment($constitutionIdOk, $addDataOk);
		$this->assertEquals(true, $result);
	}

	/**
	 * testGetCommentByConstitutionId method
	 *
	 * @return void
	 */
	public function testGetCommentByConstitutionId() {
		// null値の場合
		$result = $this->ConstitutionComment->getCommentByConstitutionId();
		$this->assertEquals(false, $result);

		// 正しい引数の場合
		$commentData = array(
			0 => array(
				'ConstitutionComment' => array(
					'body' => 'Lorem ipsum dolor sit amet',
					'username' => 'Lorem ipsum dolor sit amet',
				)
			),
			1 => array(
				'ConstitutionComment' => array(
					'body' => 'Lorem ipsum dolor sit amet',
					'username' => 'Lorem ipsum dolor sit amet',
				)
			)
		);
		$result = $this->ConstitutionComment->getCommentByConstitutionId(1);
		$this->assertEquals(true, !empty($result));
		$this->assertEquals($commentData, $result);

		// コメントが投稿されていなかった場合
		$result = $this->ConstitutionComment->getCommentByConstitutionId(20);
		$this->assertEquals(true, empty($result));
	}

	/**
	 * __outputCharacter
	 *
	 * @param  string  $characterNumber  文字数
	 * @return $chart  指定された文字数の文字
	 * @author mishima.masami
	 */
	private function __outputCharacter($characterNumber) {
		return str_repeat('a', $characterNumber);
	}

	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown() {
		unset($this->ConstitutionComment);

		parent::tearDown();
	}

}
