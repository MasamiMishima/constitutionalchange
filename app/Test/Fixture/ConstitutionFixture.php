<?php
/**
 * ConstitutionFixture
 *
 */
class ConstitutionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'body' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'approval_status' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'chapter_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'chapter_id' => array('column' => 'chapter_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'body' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi',
			'chapter_id' => 1,
			'approval_status' => 2,
			'created' => '2015-08-18 14:10:24',
			'modified' => '2015-08-18 14:10:24'
		),
		array(
			'id' => 2,
			'body' => 'Lorem ipsum dolor sit amet, aliquet feugiat.',
			'chapter_id' => 2,
			'approval_status' => 2,
			'created' => '2015-08-18 14:10:24',
			'modified' => '2015-08-18 14:10:24'
		),
		array(
			'id' => 3,
			'body' => 'Lorem ipsum dolor sit amet,aaaaaaaaaaaaaaaaaaaa',
			'chapter_id' => 2,
			'approval_status' => 1,
			'created' => '2015-08-18 14:10:24',
			'modified' => '2015-08-18 14:10:24'
		),
		array(
			'id' => 4,
			'body' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi',
			'chapter_id' => 6,
			'approval_status' => 0,
			'created' => '2015-08-18 14:10:24',
			'modified' => '2015-08-18 14:10:24'
		),
		array(
			'id' => 5,
			'body' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi',
			'chapter_id' => 5,
			'approval_status' => 0,
			'created' => '2015-08-18 14:10:24',
			'modified' => '2015-08-18 14:10:24'
		),
		array(
			'id' => 6,
			'body' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi',
			'chapter_id' => 4,
			'approval_status' => 0,
			'created' => '2015-08-18 14:10:24',
			'modified' => '2015-08-18 14:10:24'
		),
	);

}
