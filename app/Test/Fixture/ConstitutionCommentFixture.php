<?php
/**
 * ConstitutionCommentFixture
 *
 */
class ConstitutionCommentFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'body' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'username' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'show_status' => array('type' => 'boolean', 'null' => false, 'default' => '1'),
		'constitution_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'idx_zip' => array('column' => 'constitution_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'body' => 'Lorem ipsum dolor sit amet',
			'username' => 'Lorem ipsum dolor sit amet',
			'show_status' => 1,
			'constitution_id' => 1,
			'created' => '2015-08-18 14:06:29',
			'modified' => '2015-08-18 14:06:29'
		),
		array(
			'id' => 2,
			'body' => 'Lorem ipsum dolor sit amet',
			'username' => 'Lorem ipsum dolor sit amet',
			'show_status' => 1,
			'constitution_id' => 1,
			'created' => '2015-08-18 14:06:29',
			'modified' => '2015-08-18 14:06:29'
		),
		array(
			'id' => 3,
			'body' => 'Lorem ipsum dolor sit amet',
			'username' => 'Lorem ipsum dolor sit amet',
			'show_status' => 1,
			'constitution_id' => 2,
			'created' => '2015-08-18 14:06:29',
			'modified' => '2015-08-18 14:06:29'
		),
	);

}
