<?php
/**
 * ConstitutionModificationIdeaFixture
 *
 */
class ConstitutionModificationIdeaFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'title' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'reason' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'show_status' => array('type' => 'boolean', 'null' => false, 'default' => '1'),
		'username' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'constitution_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'constitution_id' => array('column' => 'constitution_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'title' => 'Lorem ipsum dolor sit amet',
			'reason' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'show_status' => 1,
			'username' => 'Lorem ipsum dolor sit amet',
			'constitution_id' => 1,
			'created' => '2015-08-18 14:09:19',
			'modified' => '2015-08-18 14:09:19'
		),
		array(
			'id' => 2,
			'title' => 'Lorem ipsum dolor sit amet',
			'reason' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id ',
			'show_status' => 1,
			'username' => 'Lorem ipsum dolor sit amet',
			'constitution_id' =>5,
			'created' => '2015-08-18 14:09:19',
			'modified' => '2015-08-18 14:09:19'
		),
		array(
			'id' => 3,
			'title' => 'Lorem ipsum dolor sit amet',
			'reason' => 'Lorem',
			'show_status' => 1,
			'username' => 'Lorem ipsum dolor sit amet',
			'constitution_id' => 4,
			'created' => '2015-08-18 14:09:19',
			'modified' => '2015-08-18 14:09:19'
		),

	);

}
