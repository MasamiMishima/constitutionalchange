ConstitutionChange
====

憲法の学習や変更をできます。

わかりにくい箇所を修正ししていくことができます。

## Requirement

```
php
PHPUnit
Apache
MySQL
```

## Install
+ ドキュメントルートに設置してください。
+ /app/webroot/sqlの中身をデータベースにインサートしてください

## Licence

[MIT](https://github.com/tcnksm/tool/blob/master/LICENCE)

## Author

[Mishima.Masami](https://bitbucket.org/MasamiMishima)

## Other
[![Latest Stable Version](https://poser.pugx.org/cakephp/cakephp/v/stable.svg)](https://packagist.org/packages/cakephp/cakephp)
[![License](https://poser.pugx.org/cakephp/cakephp/license.svg)](https://packagist.org/packages/cakephp/cakephp)
[![Bake Status](https://secure.travis-ci.org/cakephp/cakephp.png?branch=master)](http://travis-ci.org/cakephp/cakephp)
[![Code consistency](http://squizlabs.github.io/PHP_CodeSniffer/analysis/cakephp/cakephp/grade.svg)](http://squizlabs.github.io/PHP_CodeSniffer/analysis/cakephp/cakephp/)

[![CakePHP](http://cakephp.org/img/cake-logo.png)](http://www.cakephp.org)
